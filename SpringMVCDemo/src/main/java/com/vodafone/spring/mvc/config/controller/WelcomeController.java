package com.vodafone.spring.mvc.config.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.vodafone.spring.mvc.config.database.AuthorDatabase;
import com.vodafone.spring.mvc.config.entity.Author;

@Controller
public class WelcomeController {

	@Autowired
	AuthorDatabase db;
	
	@GetMapping("/welcome")
	public String dashboard(HttpSession session, Map<String, String> map
			,Map<String, List<Author>> mapList)
	{
		String email = (String)session.getAttribute("email");
		if(email != null) {
			map.put("email", email);
			mapList.put("authors", db.getAuthors());
			return "welcome";
		}
		return "redirect:signin";
	}
}
