package com.vodafone.spring.mvc.config.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

// tells spring that Author class object needs to be craeted by the spring framework
// and make it available for use within the spring application context

public class Author {
	private int aid;
	private String aname;
	private String city;
	private String genre;
	
	public Author() {
		System.out.println("Author constructor");
	}

	public Author(int aid, String aname, String city, String genre) {
		System.out.println("Author parameterized constructor");
		this.aid = aid;
		this.aname = aname;
		this.city = city;
		this.genre = genre;
	}


	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		System.out.println("set aid");
		this.aid = aid;
	}

	public String getAname() {
		return aname;
	}

	public void setAname(String aname) {
		this.aname = aname;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	@Override
	public String toString() {
		return "Author [aid=" + aid + ", aname=" + aname + ", city=" + city + ", genre=" + genre + "]";
	}
	

}
