package com.vodafone.spring.mvc.config.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.vodafone.spring.mvc.config.entity.Author;



// for communicating with database => repository
@Repository // schematic meaning
public class AuthorDatabase {

	@Autowired
	private JdbcTemplate template;
	
	
	public AuthorDatabase() {
		System.out.println("Author database constructor");
		
	}
	public long count()
	{
		String sql = "select count(*) from author";
		return this.template.queryForObject
				(sql, Long.class);
	}
	public boolean deleteAuthor(int id)
	{
		String sql = "delete from author where aid=?";
		try {
			this.template.update(sql, id);
		}
		catch(DataAccessException e)
		{
			System.out.println(e);
			return false;
		}
		return true;
	}
	public boolean insertAuthor(Author author)
	{
		String sql = "insert into author values(?,?,?,?)";
		try {
			this.template.update(sql, author.getAid(), author.getAname(),
					author.getCity(), author.getGenre());
		}
		catch(DataAccessException e){
			System.out.println(e);
			return false;
		}
		return true;
	}
	public Author getAuthorById(int id)
	{
		String sql = "select * from author where aid=?";
		return this.template.queryForObject(sql, new AuthorRowMapper(), id);
	}
	public List<Author> getAuthors()
	{
		String sql = "select * from author";
		return this.template.query(sql, new AuthorRowMapper());
	}
	
	class AuthorRowMapper implements RowMapper<Author>
	{
		@Override
		public Author mapRow(ResultSet rs, int rowNum) throws SQLException {
			// TODO Auto-generated method stub
			Author author = new Author();
			author.setAid(rs.getInt(1));
			author.setAname(rs.getString(2));
			author.setCity(rs.getString(3));
			author.setGenre(rs.getString(4));
			return author;
		}
		
	}
}

