package com.techgatha.loan.LoanProposalSpringBootProject.repository;

import org.springframework.data.repository.CrudRepository;

import com.techgatha.loan.LoanProposalSpringBootProject.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer	,String>{

}
