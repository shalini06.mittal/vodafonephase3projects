package com.techgatha.loan.LoanProposalSpringBootProject.springservice;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techgatha.loan.LoanProposalSpringBootProject.exception.EntityNotFoundException;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Customer;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Employee;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Loan;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.CustomerRepository;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.EmployeeRepository;
import com.techgatha.loan.LoanProposalSpringBootProject.repository.LoanRepository;


@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private LoanRepository loanRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Transactional
	public void approveLoan(String eid) throws Exception {

		if(searchEmployeeById(eid)) {

			ArrayList<Loan> loans = loanRepository.findByEmployeeEmployeeId(eid);
			if(loans.size()>0) {
				for(Loan loan:loans) {
					Customer customer = loan.getCustomer();
					System.out.println("get collaterals "+loan.getCollaterals());
					if(loan.getLoanAmount() > (10 * customer.getAnnualIncome())) {
						loan.setRemarks("Loan amount cannot be 10 times of annual income");
						loan.setApproved(false);
						this.loanRepository.save(loan);
						//LoanDAO.updateLoanStatus(false, "Loan amount cannot be 10 times of annual income", loan.getLoanId());
					}
					else if(loan.getCollaterals() == null || loan.getCollaterals().size() == 0) {
						loan.setRemarks( "No collateral submitted");
						loan.setApproved(false);
						this.loanRepository.save(loan);
						//LoanDAO.updateLoanStatus(false, "No collateral submitted", loan.getLoanId());
					}
					else if(!customer.isIncomeTaxReturnAttached()) {
						loan.setRemarks( "Income proof not attached");
						loan.setApproved(false);
						this.loanRepository.save(loan);
						//LoanDAO.updateLoanStatus(false, "Income proof not attached", loan.getLoanId());
					}
					else if(customer.getCustomerIdentity() == null) {
						loan.setRemarks( "Identity document not submitted");
						loan.setApproved(false);
						this.loanRepository.save(loan);
						//LoanDAO.updateLoanStatus(false, "Identity document not submitted", loan.getLoanId());
					}
					else
					{
						loan.setRemarks( "Approved");
						loan.setApproved(true);
						
						this.loanRepository.save(loan);
						//LoanDAO.updateLoanStatus(true, "Approved", loan.getLoanId());
					}
				}
			}
			else {
				throw new Exception("No loan proposals against this employee id");
			}
		}
		else
			throw new EntityNotFoundException("Employee does not exist");
	}
	public boolean searchEmployeeById(String empid)
	{
		return !this.employeeRepository.findById(empid).isEmpty();

	}
	
	public boolean addEmployee(Employee employee) {
		if(this.employeeRepository.findById(employee.getEmployeeId()).orElse(null) == null)
			try{
				this.employeeRepository.save(employee);
			}
		catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}

}
