package com.techgatha.loan.LoanProposalSpringBootProject.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.techgatha.loan.LoanProposalSpringBootProject.model.Loan;
import com.techgatha.loan.LoanProposalSpringBootProject.springservice.CustomerService;

@Controller
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	
	@GetMapping()

	public String details(HttpSession session, Map<String, List<Loan>> map)
	{
		String email = (String) session.getAttribute("custemail");
		System.out.println("rediect "+email);
		List<Loan> loans = this.customerService.getAllLoansWithCustomerId(email);
		//System.out.println(loans.get(0).getCollaterals());
		map.put("loans", loans);
		return "dashboard";
	}
	
	@GetMapping("/profile")
	public String profile()
	{
		return "profile";
	}
	
}
