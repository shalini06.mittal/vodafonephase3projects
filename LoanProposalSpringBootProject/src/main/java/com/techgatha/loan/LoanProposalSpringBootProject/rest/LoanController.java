package com.techgatha.loan.LoanProposalSpringBootProject.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.techgatha.loan.LoanProposalSpringBootProject.exception.EntityNotFoundException;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Customer;
import com.techgatha.loan.LoanProposalSpringBootProject.model.Loan;
import com.techgatha.loan.LoanProposalSpringBootProject.model.ResponseLoan;
import com.techgatha.loan.LoanProposalSpringBootProject.model.User;
import com.techgatha.loan.LoanProposalSpringBootProject.response.ResponsePage;
import com.techgatha.loan.LoanProposalSpringBootProject.springservice.CustomerService;
import com.techgatha.loan.LoanProposalSpringBootProject.springservice.LoanService;


@RestController
@RequestMapping("/loan")
@CrossOrigin
public class LoanController {

	@Autowired
	private LoanService loanservice;

	@Autowired
	private CustomerService service;

	@PostMapping("/login")
	public ResponsePage login(@RequestBody User user)
	{
		System.out.println("post user "+user);
		Customer customer;
		try {
			customer = this.service.findCustomerByEmail(user.getEmail());
			if(customer.getPassword().equals(user.getPassword())) {

				return new ResponsePage(200, "SUCCESS");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			return new ResponsePage(401, "FAILURE");
		}
		return new ResponsePage(200, "SUCCESS");
	}

	@PostMapping("/{email}")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Loan applyLoan(
			@RequestBody Loan loan, @PathVariable String email)
	{
		return this.loanservice.applyForLoan(loan.getLoanType(),
				loan.getLoanAmount(), loan.getPeriod(), email);
	}
	//http://localhost:8080/loan/upload/ABKB001_shalini@gmail.com?ids=C001&ids=C002
	@PostMapping("/upload/{loanid}")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<ResponsePage> uploadCollaterals(
			@PathVariable String loanid,  
			@RequestParam List<String> ids)
	{
		System.out.println(ids);
		System.out.println(loanid);
		if(this.loanservice.uploadCollateral(loanid, ids))
			return ResponseEntity.status(HttpStatus.CREATED)
					.body(new ResponsePage(201, "Collaterals Updated"));
		return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
				.body(new ResponsePage(201, "Collaterals were not updated"));
	}
	@GetMapping("/{loanid}")
	public ResponseLoan getLoanById(@PathVariable String loanid)
	{
		try {
			Loan loan =  this.loanservice.findLoanById(loanid);
			ResponseLoan loanresp = new ResponseLoan(); 
			loanresp.setApproved(loan.isApproved());
			loanresp.setCollaterals(loan.getCollaterals());
			loanresp.setEmployeeId(loan.getEmployee().getEmployeeId());
			loanresp.setInterestRate(loan.getInterestRate());
			loanresp.setLoanAmount(loan.getLoanAmount());
			loanresp.setLoanId(loan.getLoanId());
			loanresp.setLoanType(loan.getLoanType());
			loanresp.setPeriod(loan.getPeriod());
			loanresp.setRemarks(loan.getRemarks());
			return loanresp;
		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	@GetMapping("/fetch/{email}")
	public ResponseEntity<Object> details(@PathVariable String email)
	{

		System.out.println("details "+email);
		List<Loan> loans = this.service.getAllLoansWithCustomerId(email);
		List<ResponseLoan> loansResp = new ArrayList<ResponseLoan>();
		if(loans.size() > 0 ) {
			loansResp = loans.stream().map(loan -> {
				ResponseLoan loanresp = new ResponseLoan();
//				loanresp.setApproved(loan.isApproved());
//				loanresp.setCollaterals(loan.getCollaterals());
//				loanresp.setEmployeeId(loan.getEmployee().getEmployeeId());
//				loanresp.setInterestRate(loan.getInterestRate());
//				loanresp.setLoanAmount(loan.getLoanAmount());
				loanresp.setLoanId(loan.getLoanId());
				loanresp.setLoanType(loan.getLoanType());
//				loanresp.setPeriod(loan.getPeriod());
//				loanresp.setRemarks(loan.getRemarks());
				return loanresp;
			}).collect(Collectors.toList());
			return ResponseEntity.status(HttpStatus.OK).body(loansResp);
		}
		//System.out.println(loans.get(0).getCollaterals());

		
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body("FAILURE");
	}

}
