<%@page import="com.techgatha.loan.LoanProposalSpringBootProject.model.Loan"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix='c'%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<style>
.bg1 {
	background-color: #62316a !important;
}

.bg1 a {
	color: white !important;
}

.loans {
	padding: 30px;
	background-color: #c2b0c4;
	margin: 20px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light bg1">
		<div class="container-fluid">
			<a class="navbar-brand" href="#">ABKB Bank</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNav"
				aria-controls="navbarNav" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link active"
						aria-current="page" href="#">Welcome <c:out
								value="${sessionScope.custemail }"></c:out></a></li>
					<li class="nav-item"><a class="nav-link" href="/customers/profile">Profile</a>
					</li>
					<li class="nav-item"><a class="nav-link" href="/logout">Logout</a>
					</li>


				</ul>
			</div>
		</div>
	</nav>


	<c:if test="${sessionScope.custemail == null }">
		<c:redirect url="/login"></c:redirect>
	</c:if>
	<%
		List<Loan> loans = (List<Loan>)request.getAttribute("loans");
		if(loans.size()<=0)
			out.println("<h1>No Loans Proposals Yet</h1>");
		else{
	%>
		

	
	<div class='container'>
		<c:forEach items="${loans }" var="loan">
			<div class='loans'>
				<div class='row'>
					<div class='col-md-3'>Loan Id:</div>
					<div class='col-md-4'>${loan.loanId }</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>Loan Type:</div>
					<div class='col-md-4'>${loan.loanType }</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>Loan Amount :</div>
					<div class='col-md-4'>${loan.loanAmount }</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>Interest Rate :</div>
					<div class='col-md-4'>${loan.interestRate }</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>Loan Period:</div>
					<div class='col-md-4'>${loan.period }</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>Employee Id Assigned:</div>
					<div class='col-md-4'>${loan.employee.employeeId }</div>
				</div>
				<div class='row'>
					<div class='col-md-3'>Collaterals : </div>
					<div class='col-md-4'>
					<c:forEach items="${loan.collaterals }" var="coll">
					<div>${coll.collateralType }</div>
					</c:forEach>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-12'>Remarks: ${loan.remarks }</div>
				</div>
				<div class='row text-right'>
					<div class='col-md-12'>Approved : ${loan.approved }</div>
				</div>
			</div>
		</c:forEach>
	</div>
	<%} %>
</body>
</html>
