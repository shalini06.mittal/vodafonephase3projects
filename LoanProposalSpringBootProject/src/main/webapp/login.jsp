<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<link href='css/login.css' type='text/css' rel='stylesheet'/>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand" href="#">ABKB Bank</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNav"
				aria-controls="navbarNav" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link active"
						aria-current="page" href="/register">Register</a></li>
					<li class="nav-item"><a class="nav-link" href="/login">Login</a>
					</li>
					
					
				</ul>
			</div>
		</div>
	</nav>
	
	<div>
		<div class="wrapper fadeInDown">
			<div id="formContent">
				<div class="fadeIn first">
					<h2 class='sign'>Sign In</h2>
				</div>
				<form action="login" method="POST">
					<input type="text" id="login" class="fadeIn second" name="email"
						placeholder="Email"
						value="kshitij@gmail.com"/> 
						<input type="password" value="kshitij"
						id="password" class="fadeIn third" name="password"
						placeholder="Password"/> <input type="submit"
						class="fadeIn fourth" value="Log In" />
				</form>
				
			</div>
		</div>
	</div>
	
</body>
</html>

