package com.vodafone.spring.SpringCoreJdbcDemo;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.vodafone.spring.SpringCoreJdbcDemo.database.AuthorDatabase;
import com.vodafone.spring.SpringCoreJdbcDemo.entity.Author;

@Configuration
@ComponentScan(basePackages = {"model","com.vodafone.spring.SpringCoreJdbcDemo"})
@PropertySource("classpath:db.properties")
public class AppConfig {

	@Value("${driver}")
	private String driver;
	@Value("${url}")
	private String url;
	@Value("${username}")
	private String username;
	@Value("${password}")
	private String password;
	
	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext context 
		= new AnnotationConfigApplicationContext(AppConfig.class);
//		AuthorDatabase db = context.getBean(AuthorDatabase.class);
//		System.out.println(db.count());
//		Author a1 = new Author(1, "SM", "Delhi", "Suspense");
//		//System.out.println(db.insertAuthor(a1));
//		System.out.println(db.getAuthorById(2));
//		for(Author author:db.getAuthors())
//			System.out.println(author);
		
	}	
	@Bean
	public DataSource dataSource()
	{
		System.out.println("datasource conenction parameters");
		System.out.println(driver);
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName(driver);
		ds.setUrl(url);
		ds.setUsername(username);
		ds.setPassword(password);
		return ds;
	}
	@Bean
	public JdbcTemplate template()
	{
		return new JdbcTemplate(dataSource());
	}

}
