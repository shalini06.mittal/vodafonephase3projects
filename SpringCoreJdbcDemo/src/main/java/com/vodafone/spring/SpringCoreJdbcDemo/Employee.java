package com.vodafone.spring.SpringCoreJdbcDemo;

import org.springframework.stereotype.Component;

@Component
public class Employee {

	public Employee() {
		System.out.println("employee constructor");
	}
}
