package com.vodafone.spring.SpringCoreJdbcDemo.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

// tells spring that Author class object needs to be craeted by the spring framework
// and make it available for use within the spring application context
@Component
@Scope(scopeName = "prototype")
public class Author {
	// field injection
	//@Value("1")
	private int aid;
	//@Value("Ellon Musk")
	private String aname;
	//@Value("SFO")
	private String city;
	//@Value("Technical")
	private String genre;
	
	public Author() {
		System.out.println("Author constructor");
	}

	
	// constructor injection
//	@Autowired
//	public Author(@Value("101") int aid, @Value("SM") String aname,
//			@Value("MUM") String city, @Value("IT") String genre) {
//		System.out.println("Author parameterized constructor");
//		this.aid = aid;
//		this.aname = aname;
//		this.city = city;
//		this.genre = genre;
//	}

	public Author(int aid, String aname, String city, String genre) {
		System.out.println("Author parameterized constructor");
		this.aid = aid;
		this.aname = aname;
		this.city = city;
		this.genre = genre;
	}


	public int getAid() {
		return aid;
	}

	// setter injection
	@Value("1001")
	public void setAid(int aid) {
		System.out.println("set aid");
		this.aid = aid;
	}

	public String getAname() {
		return aname;
	}

	@Value("RS")
	public void setAname(String aname) {
		this.aname = aname;
	}

	public String getCity() {
		return city;
	}

	@Value("Chicago")
	public void setCity(String city) {
		this.city = city;
	}

	public String getGenre() {
		return genre;
	}

	@Value("Thriller")
	public void setGenre(String genre) {
		this.genre = genre;
	}

	@Override
	public String toString() {
		return "Author [aid=" + aid + ", aname=" + aname + ", city=" + city + ", genre=" + genre + "]";
	}
	

}
