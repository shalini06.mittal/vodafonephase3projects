package com.vodafone.spring.SpringCoreJdbcDemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.vodafone.spring.SpringCoreJdbcDemo.entity.Author;
import com.vodafone.spring.SpringCoreJdbcDemo.entity.Book;

// spring related configuration  => annotation based configuration
// 1. which class provides spring related configuration
@Configuration
// 2. which packages to scan for the annotations like: @Component, @Bean, @Repository,
// @Service, @Controller, @RestController etc so the the object of the classes can be created
@ComponentScan//(basePackages = "com.vodafone.spring.SpringCoreJdbcDemo")
public class App 
{
    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext context = 
        		new AnnotationConfigApplicationContext(App.class);
        System.out.println("Spring context created");
        // singleton only 1 instance per application is created
//        Author author = context.getBean(Author.class);
//        System.out.println(author.getAname());//1 RS
       
        Book book = context.getBean(Book.class);
        System.out.println(book);
        
//        Author author1 = context.getBean(Author.class);
//        System.out.println(author1.getAname());//2 RS
//        
//        author.setAname("YK");
//        
//        System.out.println(author.getAname());//3 YK
//        System.out.println(author1.getAname());//4 YK 
//        Author a1 = new Author();
//        System.out.println(a1);
    }
}
