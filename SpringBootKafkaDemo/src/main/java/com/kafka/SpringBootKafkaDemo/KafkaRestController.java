package com.kafka.SpringBootKafkaDemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaRestController {

	@Autowired
	private KafkaSender sender;
	
	@GetMapping("/kafka")
	public String sendMessage(@RequestParam(required = false) String message)
	{
		this.sender.send(message);
		return "message succesfully sent to the topic";
	}
}
