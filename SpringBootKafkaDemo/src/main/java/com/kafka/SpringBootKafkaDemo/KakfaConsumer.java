package com.kafka.SpringBootKafkaDemo;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KakfaConsumer {

	@KafkaListener(topics = KafkaConstants.TOPIC, groupId = "group-id")
	public void consume(String message)
	{
		System.out.println("Message Recieved => "+message);
	}
}
