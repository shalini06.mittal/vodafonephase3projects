package com.eduramp.ms.CurrencyExchangeService.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.eduramp.ms.CurrencyExchangeService.model.ConversionBean;

@RestController
public class CurrencyExchangeController {
	/*
	 * 1) RestTemplate is created , this should be autowired
	 * 2) n no of services running in a distributed environement
	 * 3) urls are dynamic , cannot have fixed urls
	 * 4) communicate, discover, available
	 * 5) monitor the health 
	 * 6) load balancer
	 * Eureka Server => cloud server
	 */
	@RequestMapping("/exchange/from/{from}/to/{to}/quantity/{quantity}")
	public ConversionBean calculateAmount(
			@PathVariable String from,
			@PathVariable String to,
			@PathVariable BigDecimal quantity)
	{
		String url = "http://localhost:8000/currency-exchange/from/{from}/to/{to}";
		RestTemplate template = new RestTemplate();
		Map<String, String> map = new HashMap<>();
		map.put("from", from);
		map.put("to", to);
		
		ConversionBean bean = template.getForObject(url, ConversionBean.class, map);
		bean.setQuantity(quantity);
		bean.setTotalCalculatedAmount(bean.getQuantity().multiply(bean.getConversionMultiple()));
		return bean;
	}

	
}
