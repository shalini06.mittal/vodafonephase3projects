package com.boot.db.SpringWebDemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.boot.db.SpringWebDemo.entity.Author;
import com.boot.db.SpringWebDemo.entity.Book;
import com.boot.db.SpringWebDemo.service.AuthorService;
import com.boot.db.SpringWebDemo.service.BookService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.util.JSONPObject;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

class Users{
	private int userId;
	private String tittle;
	private boolean completed;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getTittle() {
		return tittle;
	}
	public void setTittle(String tittle) {
		this.tittle = tittle;
	}
	public boolean isCompleted() {
		return completed;
	}
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
	@Override
	public String toString() {
		return "Users [userId=" + userId + ", tittle=" + tittle + ", completed=" + completed + "]";
	}
	
	
}
@SpringBootApplication
public class SpringWebDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebDemoApplication.class, args);
	}
	@Autowired 
	private AuthorService aservice;
	
	@Autowired 
	private BookService bservice;
	
	@Bean 
	public void getData()
	{
		RestTemplate template = new  RestTemplate();
//		ResponseEntity<String> resp = template.getForEntity("https://jsonplaceholder.typicode.com/todos/1", String.class);
//		String data  = resp.getBody();
//		System.out.println(data);
	
		ResponseEntity<Users> resp = template.getForEntity("https://jsonplaceholder.typicode.com/todos/1", Users.class);
		System.out.println(resp.getBody());
	}
	@Bean
	public void initialize()
	{
		Author a1 = new Author(1, "RS", "Mumbai", "Suspense");
		Author a2 = new Author(2, "RS", "Delhi", "IT");
		Author a3 = new Author(3, "RS", "Mumbai", "IT");
		aservice.insertAuthor(a1);
		aservice.insertAuthor(a2);
		aservice.insertAuthor(a3);
		
		Book b1 = new Book("B001", "Twin Lights",a1, 234.23);
		Book b2 = new Book("B002", "Core Java", a2, 134.23);
		Book b3 = new Book("B003", "Adventures of Tom", a1, 543.23);
		Book b4 = new Book("B004", "HTML5", a3, 123.23);
		bservice.insertBook(b1);
		bservice.insertBook(b2);
		bservice.insertBook(b3);
		bservice.insertBook(b4);
	}
	
	@Bean
	public OpenAPI openAPi()
	{
		return new OpenAPI()
				.info(new Info()
						.description("This is website to test swagger")
						.title("Author Repository")
						.contact(new Contact().email("shalini@gmail.com").name("TechGatha"))
						);
	}
}
