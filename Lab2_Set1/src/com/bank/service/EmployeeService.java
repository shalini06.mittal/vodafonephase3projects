package com.bank.service;

import java.util.ArrayList;

import com.bank.DAO.CustomerStoreItem;
import com.bank.DAO.DataStore;
import com.bank.exception.EntityNotFoundException;
import com.bank.model.Customer;
import com.bank.model.Employee;
import com.bank.model.Loan;

public class EmployeeService {


	public void approveLoan(String eid) throws EntityNotFoundException {

		if(findEmployeeByEmail(eid)) {
			ArrayList<Loan> loans = new ArrayList<Loan>();
			for(Loan loan:DataStore.getLoansProposals()) {
				if(loan.getEmployee().getEmployeeId().equals(eid))
					loans.add(loan);
			}
			for(Loan loan:loans) {
				if(loan.getLoanAmount() > (10 * loan.getCustomer().getAnnualIncome())) 
					loan.setRemarks("Loan amount cannot be 10 times of annual income");
				else if(loan.getCollaterals() == null)
					loan.setRemarks( "No collateral submitted");
				else if(!loan.getCustomer().isIncomeTaxReturnAttached())
					loan.setRemarks( "Income proof not attached");
				else if(loan.getCustomer().getCustomerIdentity() == null)
					loan.setRemarks( "Identity document not submitted");
				else
				{
					loan.setRemarks( "Approved");
					loan.setApproved(true);;
				}
			}
		}
		else
			throw new EntityNotFoundException(eid+" Employee id is invalid");
	}
	public boolean findEmployeeByEmail(String empid)
	{

		System.out.println("empid "+empid);
		ArrayList<Employee> employees = DataStore.getEmployees();
		for(Employee employee : employees)
		{
			if(employee.getEmployeeId().equals(empid)) {
				return true;
			}
		}

		return false;
	}
}
