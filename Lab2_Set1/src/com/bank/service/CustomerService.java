package com.bank.service;

import java.util.ArrayList;
import java.util.List;

import com.bank.DAO.DataStore;
import com.bank.DAO.CustomerStoreItem;
import com.bank.constants.LoanConstants;
import com.bank.exception.EntityNotFoundException;
import com.bank.model.Collateral;
import com.bank.model.Customer;
import com.bank.model.Loan;
public class CustomerService {

	public Customer findCustomerByEmail(String email) throws EntityNotFoundException
	{
		Customer customer = null;
		ArrayList<Customer> customers = CustomerStoreItem.getCustomersStored();
		for(Customer cust : customers)
		{
			if(cust.getCustomerEmailId().equals(email)) {
				customer = cust;
				break;
			}
		}
		if (customer == null)
			throw new EntityNotFoundException("Customer does not exist");
		return customer;
	}
	public boolean addCustomer(Customer customer)
	{
		try {
			ArrayList<Customer> customers = CustomerStoreItem.getCustomersStored();
			if(customers == null)
				customers = new ArrayList<Customer>();
			customers.add(customer);
			CustomerStoreItem.addCustomer(customers);
		}catch(Exception e )
		{
			return false;
		}
		return true;
	}
	public Loan uploadCollaterals(String loanId, String ...collateralTypes) throws EntityNotFoundException
	{
		Loan loan = null;
		int x = 1;
		for(Loan ln:DataStore.getLoansProposals())
		{
			if(ln.getLoanId().equals(loanId)) {
				loan = ln;
				ArrayList<Collateral> collaterals = new ArrayList<Collateral>();
				for(String collateral:collateralTypes)
					collaterals.add(new Collateral("CT00"+x++, collateral));
				loan.setCollaterals(collaterals);
				break;
			}
		}
		if (loan == null)
			throw new EntityNotFoundException("Loan Proposal not found");
		return loan;
	}
	public Loan applyForLoan(String loanType, double loanAmount,
			double period, Customer customer)
	{
		Loan loan = new Loan();
		loan.setCustomer(customer);
		loan.setInterestRate(LoanConstants.calculateRate(period));
		loan.setPeriod(period);
		loan.setLoanType(loanType);
		loan.setLoanAmount(loanAmount);
		int index = (int)(Math.random()* (DataStore.getEmployees().size()));
		loan.setEmployee(DataStore.getEmployees().get(index));
		loan.setLoanId("ABKB "+(DataStore.getLoansProposals().size()+1)+" "+customer.getCustomerEmailId());
		DataStore.getLoansProposals().add(loan);
		return loan;
	}
	
}
