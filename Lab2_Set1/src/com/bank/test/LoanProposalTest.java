package com.bank.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.bank.DAO.CustomerStoreItem;
import com.bank.DAO.DataStore;
import com.bank.constants.LoanConstants;
import com.bank.exception.EntityNotFoundException;
import com.bank.model.Collateral;
import com.bank.model.Customer;
import com.bank.model.Loan;
import com.bank.service.CustomerService;
import com.bank.service.EmployeeService;

import static com.bank.constants.LoanConstants.*;
public class LoanProposalTest {

	private Customer customer1, customer2;
	private Loan expected1, expected2;
	private ArrayList<Loan> loans;

	@Before
	public void setUp() {
		customer1 = new Customer();
		customer1.setAnnualIncome(120000);
		customer1.setCustomerEmailId("trial@gmail.com");
		customer1.setCustomerIdentity(ID_TYPE[0]);
		customer1.setIncomeTaxReturnAttached(true);

		customer2 = new Customer();
		customer2.setAnnualIncome(1020000);
		customer2.setCustomerEmailId("trial1@gmail.com");
		customer2.setCustomerIdentity(ID_TYPE[1]);
		customer2.setIncomeTaxReturnAttached(true);

		//fails for loan amount
		expected1 = new Loan();
		expected1.setCustomer(customer1);
		expected1.setInterestRate(0.06);
		expected1.setLoanAmount(12000000);
		expected1.setPeriod(3);
		expected1.setLoanId("ABKB 1 "+customer1.getCustomerEmailId());
		expected1.setLoanType(LOAN_TYPE[0]);
		expected1.setEmployee(DataStore.getEmployees().get(0));

		//success
		expected2 = new Loan();
		expected2.setCustomer(customer2);
		expected2.setInterestRate(0.08);
		expected2.setLoanAmount(10000000);
		expected2.setPeriod(5);
		expected2.setLoanId("ABKB 2 "+customer2.getCustomerEmailId());
		expected2.setLoanType(LOAN_TYPE[3]);
		expected2.setEmployee(DataStore.getEmployees().get(0));

		loans = new ArrayList<Loan>();
		loans.add(expected1);
		loans.add(expected2);
	}

	// Q1
	@Test
	public void testCalculateRateReturnsInterestRate() {

		double period[] = {1,3.5,6,9.5};
		double expected[] = {0.05, 0.06, 0.08, 0.085};
		for (int i=0;i<period.length;i++)
		{
			double actual = LoanConstants.calculateRate(period[i]);
			assertEquals(expected[i], actual,0.0);
		}
	}
	//Q1
	@Test
	public void testEmployeesExist()
	{
		int expectedSize = 3;
		String expectedId = "E001";
		int actual = DataStore.getEmployees().size();
		assertEquals(expectedSize, actual);
		assertEquals(expectedId, DataStore.getEmployees().get(0).getEmployeeId());
	}
	//Q2
	@Test
	public void testApplyLoanReturnsValidLoanId()
	{
		CustomerService service = new CustomerService();
		service.applyForLoan(LOAN_TYPE[0], 12000000, 3, customer1);
		service.applyForLoan(LOAN_TYPE[3], 12000000, 5, customer2);
		ArrayList<Loan> actual = DataStore.getLoansProposals();
		assertEquals(2, actual.size());
		assertEquals(expected1.getLoanId(), actual.get(0).getLoanId());
		assertEquals(expected2.getLoanId(), actual.get(1).getLoanId());

		DataStore.getLoansProposals().remove(0);
		DataStore.getLoansProposals().remove(0);
	}
	//Q2
	@Test(expected = EntityNotFoundException.class)
	public void testUploadCollateralsThrowsException() throws EntityNotFoundException
	{	
		CustomerService s1 = new CustomerService();
		s1.applyForLoan(LOAN_TYPE[0], 12000000, 3, customer1);
		s1.applyForLoan(LOAN_TYPE[3], 12000000, 5, customer2);
		Loan actual = s1.uploadCollaterals("ABKB 3 trial@gmail.com", COLLATERAL_TYPE[1], COLLATERAL_TYPE[2]);
		DataStore.getLoansProposals().remove(0);
		DataStore.getLoansProposals().remove(0);
	}
	//Q2
	@Test()
	public void testFindEmployeeBYEmailReturnsTrue() 
	{	
		EmployeeService service = new EmployeeService();
		
		boolean actual1 = service.findEmployeeByEmail("E001");
		boolean actual2 = service.findEmployeeByEmail("E004");
		assertTrue(actual1);
		assertFalse(actual2);
		
	}
	//Q2
	@Test
	public void testUploadCollateralsAddedSuccessfully() throws EntityNotFoundException
	{
		CustomerService s1 = new CustomerService();
		s1.applyForLoan(LOAN_TYPE[0], 12000000, 3, customer1);

		ArrayList<Collateral> collaterals = new ArrayList<Collateral>();
		collaterals.add(new Collateral("CT001", COLLATERAL_TYPE[1]));
		collaterals.add(new Collateral("CT002", COLLATERAL_TYPE[2]));

		expected1.setCollaterals(collaterals);
		Loan actual = s1.uploadCollaterals(expected1.getLoanId(), COLLATERAL_TYPE[1], COLLATERAL_TYPE[2]);
		assertEquals(expected1.getCollaterals().size(), actual.getCollaterals().size());

		DataStore.getLoansProposals().remove(0);
	}
	//Q2
	@Test
	public void testApproveLoan() throws EntityNotFoundException
	{
		DataStore.getLoansProposals().add(expected1);
		DataStore.getLoansProposals().add(expected2);

		boolean expectedApprovedLoan1 = false;
		boolean expectedApprovedLoan2 = true;
		String expectedRemarks1 = "Loan amount cannot be 10 times of annual income";
		String expectedRemarks2 = "Approved";

		CustomerService s1 = new CustomerService();
		s1.uploadCollaterals(expected1.getLoanId(), COLLATERAL_TYPE[0],COLLATERAL_TYPE[1]);
		s1.uploadCollaterals(expected2.getLoanId(), COLLATERAL_TYPE[2],COLLATERAL_TYPE[1]);


		EmployeeService service = new EmployeeService();
		service.approveLoan(DataStore.getEmployees().get(0).getEmployeeId());

		assertEquals(expectedApprovedLoan1, DataStore.getLoansProposals().get(0).isApproved());
		assertEquals(expectedApprovedLoan2, DataStore.getLoansProposals().get(1).isApproved());
		assertEquals(expectedRemarks1, DataStore.getLoansProposals().get(0).getRemarks());
		assertEquals(expectedRemarks2, DataStore.getLoansProposals().get(1).getRemarks());

		DataStore.getLoansProposals().remove(0);
		DataStore.getLoansProposals().remove(0);

	}
	//Q3
	public void testCustomerItemsStoredAndRetrievedSuccessfully()
	{
		int expectedSize = 2;
		ArrayList<Customer> expected = new ArrayList<Customer>();
		expected.add(customer1);
		expected.add(customer2);

		CustomerStoreItem.addCustomer(expected);

		ArrayList<Customer> actual = CustomerStoreItem.getCustomersStored();
		assertEquals(expectedSize, actual.size());
	}
	//Q3
	@Test 
	public void testFindCustomerByEmailReturnsCustomer() throws EntityNotFoundException
	{
		ArrayList<Customer> customers = new ArrayList<Customer>();
		customers.add(customer1);
		customers.add(customer2);

		CustomerStoreItem.addCustomer(customers);

		String expected = customer1.getCustomerEmailId();

		CustomerService service = new CustomerService();
		Customer actual = service.findCustomerByEmail(customer1.getCustomerEmailId());
		assertEquals(expected, actual.getCustomerEmailId());
	}

}
