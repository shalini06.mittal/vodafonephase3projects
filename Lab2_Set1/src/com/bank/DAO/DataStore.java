package com.bank.DAO;

import java.util.ArrayList;

import com.bank.model.Customer;
import com.bank.model.Employee;
import com.bank.model.Loan;
import com.bank.service.CustomerService;

import static com.bank.constants.LoanConstants.ID_TYPE;

public class DataStore {

	private static ArrayList<Employee> employees = new ArrayList<Employee>();
	//private static ArrayList<Customer> customers = new ArrayList<Customer>();
	private static ArrayList<Loan> loans = new ArrayList<Loan>();
	
	static {
		employees.add(new Employee("E001", "George W. Keller"));
		employees.add(new Employee("E002", "Sarang Sengupta"));
		employees.add(new Employee("E003", "Shreya Grandhi"));
	}
	public static ArrayList<Employee> getEmployees()
	{
		return employees;
	}
	
	public static ArrayList<Loan> getLoansProposals()
	{
		return loans;
	}
	
}
