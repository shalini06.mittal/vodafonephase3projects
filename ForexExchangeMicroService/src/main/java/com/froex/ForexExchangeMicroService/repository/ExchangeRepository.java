package com.froex.ForexExchangeMicroService.repository;

import org.springframework.data.repository.CrudRepository;

import com.froex.ForexExchangeMicroService.model.CurrencyExcchange;


public interface ExchangeRepository extends CrudRepository<CurrencyExcchange, Long>{

	public CurrencyExcchange findByFromAndTo(String from,String to);
}
