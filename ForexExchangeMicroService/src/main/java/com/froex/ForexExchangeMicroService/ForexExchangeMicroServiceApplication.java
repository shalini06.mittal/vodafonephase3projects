package com.froex.ForexExchangeMicroService;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import com.froex.ForexExchangeMicroService.model.CurrencyExcchange;
import com.froex.ForexExchangeMicroService.repository.ExchangeRepository;
import com.netflix.discovery.converters.Auto;

@SpringBootApplication
@EnableEurekaClient
public class ForexExchangeMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForexExchangeMicroServiceApplication.class, args);
	}
	
	@Autowired
	private ExchangeRepository repo;
	
	@Bean
	public void insertData()
	{
		CurrencyExcchange c1 = new CurrencyExcchange(1001L, "USD", "INR", new BigDecimal(70.0));
		CurrencyExcchange c2 = new CurrencyExcchange(1002L, "EUR", "INR", new BigDecimal(80.0));
		repo.save(c1);
		repo.save(c2);
	}
}
