package com.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;

import java.time.Duration;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.demos.MathUtils;

@TestMethodOrder(OrderAnnotation.class)
class TestMathUtils {

	MathUtils utils ;
	int result1, result2; 
	
	@BeforeEach
	void setUp() throws Exception {
		utils = new MathUtils();
		result1 = 10;
		result2= -20;
	}

	@AfterEach
	void tearDown() throws Exception {
		utils = null;
	}

	@Test
	//@Disabled
	@Order(1)
	@RepeatedTest(3)
	void testAddReturnsPositiveValue() {
		System.out.println("order 1");
		assertEquals(result1, utils.add(5, 5));
	}
	
	@Test
	@DisplayName("Add Returns Negative Value")
	@Order(4)
	void testAddReturnsNegativeValue() {
		System.out.println("order 4");
		assertEquals(result2, utils.add(-5, -15));
	}
	@Test
	@Order(3)
	public void testFactorialThrwosException()
	{
		System.out.println("order 3");
		assertThrows(RuntimeException.class, ()->{
			utils.factorial(-3);
		}, "Should throw an exception");
	}
//	@Test
//	@Order(2)
	public void testTimeOut()
	{
		assertTimeout(Duration.ofMillis(2100),() -> utils.fetchData());
	}

}
