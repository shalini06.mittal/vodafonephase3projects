package com.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.demos.LoginUser;

public class ParameterizedTests {

	LoginUser user;
	@BeforeEach
	public void setUp()
	{
		user = new LoginUser();
	}
	
	@ParameterizedTest(name = "{0} : {1} : {2}")
	@MethodSource("loginValues")
	public void testLoginValid(String email, String password, boolean expected) {
		assertEquals(expected, user.validate(email, password));
	}
	
	private static Stream<Arguments> loginValues()
	{
		return Stream.of(
				Arguments.of("e1@gmail.com", "e1", true),
				Arguments.of("e2@gmail.com", "e2", true),
				Arguments.of("e3@gmail.com", "e3", false),
				Arguments.of("e10@gmail.com", "e1", false)
				);
	}
	
}
