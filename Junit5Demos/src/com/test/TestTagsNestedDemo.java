package com.test;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

class TestTagsNestedDemo {

	@Nested
	@Tag("dev")
	class Development{
		@BeforeEach
		public void beforeAll()
		{
			System.out.println("before all");
		}
		@Test
		public void m1()
		{
			System.out.println("m1");
		}
		
	}
	
	@Nested
	@Tag("prod")
	class Production{
		@BeforeEach
		public  void beforeAll()
		{
			System.out.println("before all");
		}
		@Test
		public void m2()
		{
			System.out.println("m2");
		}
		
	}

}
