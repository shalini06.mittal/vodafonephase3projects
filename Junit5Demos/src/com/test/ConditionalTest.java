package com.test;

import static org.junit.Assert.assertNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnJre;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.JRE;
import org.junit.jupiter.api.condition.OS;

public class ConditionalTest {

	@Test
	@EnabledOnOs(value = OS.MAC)
	public void runOnMac()
	{
		assertNull(null);
	}
	@Test
	@EnabledOnOs(value = OS.WINDOWS)
	public void runOnWindows()
	{
		assertNull(null);
	}
	
	@Test
	@EnabledOnJre(value = JRE.JAVA_8)
	public void runOnJRE8()
	{
		assertNull(null);
	}
	@Test
	@EnabledOnJre(value = JRE.JAVA_11)
	public void runOnJRE11()
	{
		assertNull(null);
	}
	
}
