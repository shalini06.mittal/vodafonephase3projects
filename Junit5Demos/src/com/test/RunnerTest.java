package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.junit.runner.RunWith;

import com.demos.MathUtils;

// test suite
//@RunWith(JUnitPlatform.class)
@Suite
@IncludeTags("prod")
@SelectClasses({
	TestTagsDemo.class,
	TestMathUtils.class,
	TestTagsNestedDemo.class})
class RunnerTest {


}
