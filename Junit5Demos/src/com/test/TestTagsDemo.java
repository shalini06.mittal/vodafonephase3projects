package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Tags;
import org.junit.jupiter.api.Test;

class TestTagsDemo {

	@BeforeAll
	@Tag("dev")
	public static void beforeAll()
	{
		System.out.println("before all");
		
	}
	@BeforeAll
	@Tag("prod")
	public static void beforeAll1()
	{
		System.out.println("before all 1");	
	}
	@Test
	@Tag("dev")
	void test1Dev() {
		
		assertTrue(true);
	}
	@Test
	@Tag("prod")
	void testProd() {
		assertTrue(true);
	}
	@Test
	@Tag("dev")
	void testDev() {
		assertTrue(true);
	}
	@Test

	@Tag("prod")
	@Tag("dev")
	void testDevAndProd() {
		assertTrue(true);
	}
	@Test
	@Tag("test")
	void testTest() {
		assertTrue(true);
	}

}
