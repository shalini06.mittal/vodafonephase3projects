package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * AuthorService =>
 * insertAuthor => 1) create db and connect to  it, dummy data => beforeall and afterall
 * 2) author object // existing and new
 * update => existing and new
 * delete => id 
 * select => dummy data 
 *
 */
class JUnitBasics {

	
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("after all");
		// db close
	}

	@BeforeEach
	void setUp() throws Exception {
		System.out.println("before each");
		// creations of the existing and new author objects
	}

	@AfterEach
	void tearDown() throws Exception {
		System.out.println("after each");
		// releasing the memoru
	}

	@Test
	void test1() {
		System.out.println("test1");
		// testing whether your expectations meet when the actual call happens
		// insertAuthor => actual and expectation assertions
		assertTrue(true);
		
	}
	@Test
	void test2() {
		System.out.println("test2");
		assertTrue(true);
	}
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.out.println("before all");// db open
	}


}
