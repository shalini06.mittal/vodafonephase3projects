package com.demos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginUser {

	private Map<String, String> map = new HashMap<>();
	public LoginUser() {
		map.put("e1@gmail.com", "e1");
		map.put("e2@gmail.com", "e2");
		map.put("e3@gmail.com", "e3");
		map.put("e4@gmail.com", "e4");
		
	}
	public boolean validate(String email, String password)
	{
		if(map.containsKey(email)) {
			String pwd= map.get(email);
			if(pwd.equals(password))
				return true;
		}
		return false;
	}
}
