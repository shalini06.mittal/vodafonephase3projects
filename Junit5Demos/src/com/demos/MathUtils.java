package com.demos;

public class MathUtils {

	public int add(int a,int b)
	{
		return a+b;
	}
	public int factorial(int no)
	{
		if(no<=0)
			throw new RuntimeException("no should be positive");
		int f = 1;
		for(int i=1;i<=no;i++)
			f+=i;
		
		return f;
	}
	public String fetchData()
	{
		System.out.println("Fetching Data...");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("data fetched");
		return "SUCCESS";
	}
}
