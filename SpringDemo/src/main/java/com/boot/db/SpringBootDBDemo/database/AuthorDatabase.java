package com.boot.db.SpringBootDBDemo.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AuthorDatabase {

	public AuthorDatabase() {
		System.out.println("author database constructor");
	}
	@Autowired
	private JdbcTemplate template;

	public JdbcTemplate getTemplate() {
		return template;
	}
	
	
}
