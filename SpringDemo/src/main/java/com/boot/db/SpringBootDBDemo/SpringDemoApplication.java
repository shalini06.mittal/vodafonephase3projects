package com.boot.db.SpringBootDBDemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.boot.db.SpringBootDBDemo.database.AuthorDatabase;
import com.boot.db.SpringBootDBDemo.entity.Author;
import com.boot.db.SpringBootDBDemo.entity.Book;
import com.boot.db.SpringBootDBDemo.service.AuthorService;
import com.boot.db.SpringBootDBDemo.service.BookService;

@SpringBootApplication
public class SpringDemoApplication {

	public static void main(String[] args) {
		System.out.println("hello");
		SpringApplication.run(SpringDemoApplication.class, args);
		System.out.println("configured");
	}
	
	@Autowired
	private AuthorDatabase authordb;
	
	@Autowired 
	private AuthorService aservice;
	
	@Autowired 
	private BookService bservice;
	
	@Bean
	public void testData()
	{
		System.out.println("count of author entities "+aservice.count());
		for(Author author:aservice.getAuthorByCity("Mumbai"))
			System.out.println(author);
		
		System.out.println();
		for(Book book:bservice.getBooksByPriceGreaterThanEqualTo(200.0))
			System.out.println(book);
		
		System.out.println();
		for(Book book:bservice.getBooksByAuthor(3))
			System.out.println(book);
	}
	@Bean
	public void initialize()
	{
		System.out.println(authordb.getTemplate());
		Book b1 = new Book("B001", "Twin Lights", new Author(1, null, null, null), 234.23);
		Book b2 = new Book("B002", "Core Java", new Author(3, null, null, null), 134.23);
		Book b3 = new Book("B003", "Adventures of Tom", new Author(2, null, null, null), 543.23);
		Book b4 = new Book("B004", "HTML5", new Author(3, null, null, null), 123.23);
//		bservice.insertBook(b1);
//		bservice.insertBook(b2);
//		bservice.insertBook(b3);
//		bservice.insertBook(b4);
	}

}
