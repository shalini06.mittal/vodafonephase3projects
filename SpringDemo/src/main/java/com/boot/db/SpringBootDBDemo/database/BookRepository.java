package com.boot.db.SpringBootDBDemo.database;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.boot.db.SpringBootDBDemo.entity.Author;
import com.boot.db.SpringBootDBDemo.entity.Book;

public interface BookRepository  extends CrudRepository<Book, String>{

	// select * from book where price >= ?
	public List<Book> getByPriceGreaterThanEqual(double price);
	public List<Book> findByAuthorAid(int id);
}
